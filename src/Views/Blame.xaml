<Window x:Class="SourceGit.Views.Blame"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:controls="clr-namespace:SourceGit.Views.Controls"
        mc:Ignorable="d"
        Title="{StaticResource Text.Blame}"
        TextOptions.TextFormattingMode="Display"
        TextOptions.TextRenderingMode="ClearType"
        RenderOptions.ClearTypeHint="Enabled"
        UseLayoutRounding="True"
        WindowStartupLocation="CenterOwner"
        Height="600" Width="800">
    <WindowChrome.WindowChrome>
        <WindowChrome UseAeroCaptionButtons="False" CornerRadius="0" CaptionHeight="28" ResizeBorderThickness="1"/>
    </WindowChrome.WindowChrome>

    <controls:WindowBorder>
        <Grid>
            <Grid.RowDefinitions>
                <RowDefinition Height="28"/>
                <RowDefinition Height="1"/>
                <RowDefinition Height="24"/>
                <RowDefinition Height="*"/>
            </Grid.RowDefinitions>

            <!-- Title Bar -->
            <Grid Grid.Row="0" Background="{StaticResource Brush.TitleBar}">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto"/>
                    <ColumnDefinition Width="Auto"/>
                    <ColumnDefinition Width="*"/>
                    <ColumnDefinition Width="Auto"/>
                </Grid.ColumnDefinitions>

                <!-- Icon -->
                <Path Grid.Column="0" Margin="6,0" Width="16" Height="16" Data="{StaticResource Icon.Commit}"/>

                <!-- Title -->
                <TextBlock Grid.Column="1" Text="{StaticResource Text.Blame}"/>

                <!-- Window Commands -->
                <StackPanel Grid.Column="3" Orientation="Horizontal" WindowChrome.IsHitTestVisibleInChrome="True">
                    <controls:IconButton Click="Minimize" Width="28" Padding="8" Icon="{StaticResource Icon.Minimize}" HoverBackground="#40000000" Opacity="1"/>
                    <controls:IconButton Click="MaximizeOrRestore" Width="28" Padding="8" Icon="{StaticResource Icon.Maximize}" HoverBackground="#40000000" Opacity="1"/>
                    <controls:IconButton Click="Quit" Width="28" Padding="8" Icon="{StaticResource Icon.Close}" HoverBackground="Red" Opacity="1"/>
                </StackPanel>
            </Grid>

            <!-- Line -->
            <Rectangle 
                Grid.Row="1"
                Height="1"
                HorizontalAlignment="Stretch"
                Fill="{StaticResource Brush.Border0}"/>

            <!-- Description -->
            <Border Grid.Row="2">
                <Grid Margin="4,0">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*"/>
                        <ColumnDefinition Width="Auto"/>
                    </Grid.ColumnDefinitions>

                    <TextBlock Grid.Column="0" x:Name="txtFile" FontSize="11" Foreground="{StaticResource Brush.FG2}" FontFamily="Consolas"/>
                    <TextBlock Grid.Column="1" HorizontalAlignment="Right" Foreground="{StaticResource Brush.FG2}" FontFamily="11" Text="{StaticResource Text.Blame.Tip}"/>
                </Grid>
            </Border>
            
            <!-- Viewer -->
            <DataGrid
                Grid.Row="3"
                x:Name="blame"
                GridLinesVisibility="Vertical"
                VerticalGridLinesBrush="{StaticResource Brush.Border2}"
                BorderBrush="{StaticResource Brush.Border2}"
                BorderThickness="1"
                FrozenColumnCount="1"
                RowHeight="16"
                SelectionUnit="FullRow"
                SelectionMode="Single"
                FontFamily="Consolas"
                SizeChanged="OnViewerSizeChanged">

                <DataGrid.RowStyle>
                    <Style TargetType="{x:Type DataGridRow}" BasedOn="{StaticResource Style.DataGridRow}">
                        <EventSetter Event="RequestBringIntoView" Handler="OnViewerRequestBringIntoView"/>
                        <EventSetter Event="ContextMenuOpening" Handler="OnViewerContextMenuOpening"/>
                    </Style>
                </DataGrid.RowStyle>

                <DataGrid.Columns>
                    <DataGridTextColumn Width="Auto" IsReadOnly="True" Binding="{Binding Line.LineNumber}" ElementStyle="{StaticResource Style.TextBlock.LineNumber}"/>

                    <DataGridTemplateColumn Width="SizeToCells" MinWidth="1" IsReadOnly="True">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <Border Background="{Binding BG}" BorderThickness="0">
                                    <TextBlock Text="{Binding Line.Content}" Background="Transparent" Foreground="{StaticResource Brush.FG1}" FontFamily="Consolas,Microsoft YaHei UI" FontSize="9pt" Margin="0" Padding="2,0,0,0"/>
                                </Border>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                    </DataGridTemplateColumn>
                </DataGrid.Columns>
            </DataGrid>

            <StackPanel
                x:Name="notSupport" 
                Grid.Row="3"
                Orientation="Vertical" 
                VerticalAlignment="Center" HorizontalAlignment="Center" 
                Background="{StaticResource Brush.Window}"
                Visibility="Collapsed">
                <Path Width="64" Height="64" Data="{StaticResource Icon.Error}" Fill="{StaticResource Brush.FG2}"/>
                <TextBlock Text="{StaticResource Text.BlameTypeNotSupported}" Margin="0,16,0,0" FontFamily="Consolas" FontSize="18" FontWeight="UltraBold" HorizontalAlignment="Center" Foreground="{StaticResource Brush.FG2}"/>
            </StackPanel>

            <!-- Loading -->
            <controls:Loading x:Name="loading" Grid.Row="3" Width="48" Height="48" IsAnimating="True"/>

            <!-- Popup to show commit info -->
            <Popup x:Name="popup" Grid.Row="3" Placement="MousePoint" IsOpen="False" StaysOpen="False" Focusable="True">
                <Border BorderBrush="{StaticResource Brush.Accent1}" BorderThickness="1" Background="{StaticResource Brush.Popup}">
                    <Grid Margin="4">
                        <Grid.RowDefinitions>
                            <RowDefinition Height="24"/>
                            <RowDefinition Height="24"/>
                            <RowDefinition Height="24"/>
                        </Grid.RowDefinitions>

                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto"/>
                            <ColumnDefinition Width="Auto"/>
                        </Grid.ColumnDefinitions>

                        <TextBlock Grid.Row="0" Grid.Column="0" Text="{StaticResource Text.Blame.SHA}" Foreground="{StaticResource Brush.FG2}" Margin="4,0"/>
                        <Label Grid.Row="0" Grid.Column="1" x:Name="commitID" Margin="8,0,4,0" Padding="0" VerticalAlignment="Center"/>
                        <TextBlock Grid.Row="1" Grid.Column="0" Text="{StaticResource Text.Blame.Author}" Foreground="{StaticResource Brush.FG2}" Margin="4,0"/>
                        <TextBlock Grid.Row="1" Grid.Column="1" x:Name="authorName" Margin="8,0,4,0"/>
                        <TextBlock Grid.Row="2" Grid.Column="0" Text="{StaticResource Text.Blame.ModifyTime}" Foreground="{StaticResource Brush.FG2}" Margin="4,0"/>
                        <TextBlock Grid.Row="2" Grid.Column="1" x:Name="authorTime" Margin="8,0,4,0"/>
                    </Grid>
                </Border>
            </Popup>
        </Grid>
    </controls:WindowBorder>
</Window>
