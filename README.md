# SourceGit

开源的Git客户端，仅用于Windows。

## 下载

[发行版](https://gitee.com/sourcegit/SourceGit/releases/)

* `SourceGit.exe`为不带.NET 5.0运行时的可执行文件，需要先安装.NET 5
* `SourceGit_48.exe`为.NET 4.8编译的可执行文件，Window 10 已内置该运行时

## 预览

* DarkTheme

![Theme Dark](./screenshots/theme_dark.png)

* LightTheme

![Theme Light](./screenshots/theme_light.png)


## Thanks

* [PUMA](https://gitee.com/whgfu) 配置默认User
* [Rwing](https://gitee.com/rwing) GitFlow: add an option to keep branch after finish
* [XiaoLinger](https://gitee.com/LingerNN) 纠正弹出框文本配置方式
